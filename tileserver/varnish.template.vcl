vcl 4.0;

backend default {
  .host = "0.0.0.0";
  .port = "2080";
}

sub vcl_backend_response {
	# cache everything for 1w, ignoring any cache headers
  set beresp.ttl = 1w;
  set beresp.grace = 24h;
}

sub vcl_deliver {
  set resp.http.Access-Control-Allow-Origin = "*";
  set resp.http.Access-Control-Allow-Methods = "*";
  set resp.http.Access-Control-Allow-Headers = "Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token";
}