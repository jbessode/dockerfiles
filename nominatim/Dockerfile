FROM ubuntu:focal

ARG nominatim_version=3.5.2

# Let the container know that there is no TTY
ARG DEBIAN_FRONTEND=noninteractive

# Set locale and install packages
ENV LANG C.UTF-8

RUN apt-get -y update \
  && apt-get install -y -qq --no-install-recommends locales \
  && locale-gen en_US.UTF-8 \
  && update-locale LANG=en_US.UTF-8 \
  && apt-get install -y -qq --no-install-recommends \
  build-essential \
  cmake \
  g++ \
  libboost-dev \
  libboost-filesystem1.71-dev \
  libexpat1-dev \
  zlib1g-dev \
  libxml2-dev \
  libbz2-dev \
  libpq-dev \
  libgeos-dev \
  libgeos++-dev \
  libproj-dev \
  postgresql-server-dev-12 \
  php \
  curl \
  apache2 \
  php \
  php-pgsql \
  libapache2-mod-php \
  php-pear \
  php-db \
  php-intl \
  python3-dev \
  python3-psycopg2 \
  curl \
  ca-certificates \
  libboost-system1.71\
  libboost-filesystem1.71 \
  osmium-tool \
  sudo 

RUN apt-get clean \
  && rm -rf /var/lib/apt/lists/* \
  && rm -rf /tmp/* /var/tmp/*

# Build Nominatim
RUN cd /srv \
  && curl --silent -L http://www.nominatim.org/release/Nominatim-${nominatim_version}.tar.bz2 -o v${nominatim_version}.tar.bz2 \
  && tar xf v${nominatim_version}.tar.bz2 \
  && rm v${nominatim_version}.tar.bz2 \
  && mv Nominatim-${nominatim_version} nominatim \
  && cd nominatim \
  && mkdir build \
  && cd build \
  && cmake .. \
  && make

# Configure Nominatim
COPY local.php /srv/nominatim/build/settings/local.php

# Configure Apache
COPY nominatim.conf /etc/apache2/sites-enabled/000-default.conf

# Set the entrypoint
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh

EXPOSE 15030

CMD ["/entrypoint.sh"]