# nominatim

- This docker pull version 3.5.2 of nominatim, build app and run with apache web server
- **To run this image, it's mandatory to run postgis database before and bind a [volume](https://docs.docker.com/storage/volumes/) using docker syntax.**
- **This volumes must contains yours pbf files which will be import inside nominatim database and use for geocoding direct and indirect**.
  For exemple:

```
docker run -v {absolute path to yours directory}:/srv/data nominatim-ngms
```

- To increase import speed, you can **THREAD_NUMBER** env viraible value, the default value is 4

## Build image

```
docker build -t nominatim-ngms -f nominatim/Dockerfile nominatim
```

## Run image

```
docker run --rm -p 18080:15030 \
 -e DB_USER=root \
 -e DB_PASS=root \
 -e DB_NAME=nominatim \
 -e DB_HOST=postgis-ngms \
 -e DB_PORT=6543 \
 -e THREAD_NUMBER=6 \
 -v $(pwd)/nominatim-data:/srv/data --name nominatim-ngms nominatim-ngms

```
