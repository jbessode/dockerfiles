<?php
 // Paths
@define('CONST_Postgresql_Version', '12');
@define('CONST_Postgis_Version', '3');
 // Website settings
@define('CONST_Website_BaseURL', '/');


$db_name=getenv("DB_NAME");
$username=getenv("DB_USER");
$host=getenv("DB_HOST");
$password=getenv("DB_PASS");
$port=getenv("DB_PORT");


$format = "pgsql:host=%s;port=%s;dbname=%s;user=%s;password=%s";

$dsn = sprintf($format, $host, $port, $db_name, $username, $password); 

@define("CONST_Database_DSN", $dsn);
?>
