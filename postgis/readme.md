# postgis

- This image is base on **postgis/postgis:12-master** docker image, we add nominatim shared module inside to use with an nominatim docker

- It's can also use like any postgis database

- It's supported all **postgis:12-master** environnement variables

## Build image

```
docker build -t postgis-ngms -f postgis/Dockerfile postgis
```

## Run image

```
docker run --rm -p 6543:5432 \
 -e POSTGRES_DB=ngms \
 -e POSTGRES_USER=root \
 -e POSTGRES_PASSWORD=root \
 --name postgis-ngms postgis-ngms

```
